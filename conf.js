exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {'browserName': 'chrome'},
    specs: ['specs/dataProvider_spec.js'],
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000
    },
    onPrepare: function() {
        browser.driver.manage().window().maximize();
    }
};